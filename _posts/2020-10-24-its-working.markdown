---
layout: post
title:  "It's working!"
date:   2020-10-24 21:50:00 -0300
categories: cool
---
This is my first __from scratch__ post.

This is so *awesome*.  It makes me so happy.  Now I need to create a real website with this.  What should I put on it?  The possibilities are endless!

Let's see how well this works.